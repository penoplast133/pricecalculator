TEMPLATE = app

QT += qml quick core multimedia

android: {
INCLUDEPATH += C:\OpenCV\opencv_android\sdk\native\jni\include
LIBS += -lopencv_java
DEFINES += ANDROID_BUILD
} else {
INCLUDEPATH += C:\OpenCV\opencv\build\include
LIBS += -lopencv_highgui249
LIBS += -lopencv_imgproc249
LIBS += -lopencv_core249
DEFINES += PC_BUILD
}

SOURCES += main.cpp \
    PriceDB.cpp \
    Scanner.cpp \
    ImageProcessor.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    PriceElement.h \
    PriceDB.h \
    Scanner.h \
    ImageProcessor.h

OTHER_FILES += \
    ProjectNotice.txt
