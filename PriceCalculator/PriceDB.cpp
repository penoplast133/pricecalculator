#include "PriceDB.h"

PriceDB::PriceDB(): QAbstractListModel(), total_price(0)
{}

PriceDB::~PriceDB()
{}

void PriceDB::addElement(float price)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    price_list << PriceElement(price_list.size(), price);
    total_price += price;
    endInsertRows();
    emit totalPriceChanged();
}

bool PriceDB::deleteElement(int pos)
{
    if (price_list.size() != 0) {
        beginRemoveRows(QModelIndex(), pos, pos);
        total_price -= price_list[pos].getSum();
        price_list.removeAt(pos);
        endRemoveRows();
        emit totalPriceChanged();
        return true;
    }
    else return false;    
}

void PriceDB::clearElements()
{    
    price_list.clear();
    total_price = 0.00;
    // ...
}

void PriceDB::incCount(int pos)
{    
    if (price_list.size() != 0) {
        price_list[pos].incCount();
        total_price += price_list[pos].getPrice();        
    }
    emit dataChanged(createIndex(pos, pos), createIndex(pos, pos));
    emit totalPriceChanged();
}

void PriceDB::decCount(int pos)
{
    if (price_list.size() != 0) {
        if (price_list[pos].decCount()) {            
            total_price -= price_list[pos].getPrice();            
        }        
    }
    emit dataChanged(createIndex(pos, pos), createIndex(pos, pos));
    emit totalPriceChanged();
}

QVariant PriceDB::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() > (price_list.size()-1))
        return QVariant();

    PriceElement el = price_list[index.row()];    

    switch (role) {
        case Qt::DisplayRole:
        case idRole: return QVariant(QString::number(el.getId()));
        case countRole: return QVariant(QString::number(el.getCount()));
        case priceRole: return QVariant(QString::number(el.getPrice(), 'f', 2));
        default: return QVariant();
    }
}

int PriceDB::rowCount(const QModelIndex &parent) const
{
    return price_list.count();
}

QHash<int, QByteArray> PriceDB::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[idRole] = "id";
    roles[countRole] = "count";
    roles[priceRole] = "price";
    return roles;
}

QString PriceDB::totalPrice() const
{
    return QString::number(total_price, 'f', 2);
}
