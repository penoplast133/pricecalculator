#include "Scanner.h"
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <QString>
//#include <string>
//#include <vector>
//#include <QDebug>


Scanner::Scanner(const char *img_path): file_name(img_path)
{
}

float Scanner::Scan(int x1p, int y1p, int x2p, int y2p)
{
    ///< Загрузка изображения
    IplImage *main_image = cvLoadImage(file_name, 1);

#ifdef ANDROID_BUILD
    ///< Поворот изображения на 90 градусов по часовой стрелке
    double angle = 90;
    double scale = 1;
    CvMat *rot_mat = cvCreateMat(2, 3, CV_32FC1);
    CvPoint2D32f cntr = cvPoint2D32f(main_image->width/2.0, main_image->height/2.0);
    cv2DRotationMatrix(cntr, angle, scale, rot_mat);
    IplImage *temp_image = cvCreateImage(cvSize(main_image->width, main_image->height), main_image->depth, main_image->nChannels);
    cvWarpAffine(main_image, temp_image, rot_mat);
    cvCopy(temp_image, main_image, NULL);
    cvSaveImage("/storage/emulated/0/DCIM/image_rotated.jpg", temp_image);
    cvReleaseImage(&temp_image);
    cvReleaseMat(&rot_mat);
#endif

    if (main_image == NULL)
        return 0.0;

    ///< Обработка выделенной области (и ее преобразование к отдельному изображению)?
    int x1 = main_image->width/100 * x1p;
    int y1 = main_image->height/100 * y1p;
    int x2 = main_image->width/100 * x2p;
    int y2 = main_image->height/100 * y2p;

    cvSetImageROI(main_image, cvRect(x1, y1, x2-x1, y2-y1));

#ifdef PC_BUILD
    IplImage *image = cvLoadImage("price/price10.jpg", 1);
    //cvMotionRestore(image, image);
    //double kernel[9] = {0.1111, -0.8889, 0.1111, -0.8889, 4.1111, -0.8889, 0.1111, -0.8889, 0.1111};
    //CvMat kernel_matrix = cvMat(3, 3, CV_64FC1, kernel);
    //cvFilter2D(image, image, &kernel_matrix, cvPoint(-1,-1));

    //cvAddWeighted(image, 2, image, -0.5, 0, image);
    //cvSaveImage("Origin_image_ex.jpg", image);
    //cvEqualizeHist(image, image);
    IplImage *image_gray = cvCreateImage( cvSize(image->width, image->height), 8, 1);
#else
    IplImage *image = cvCreateImage(cvGetSize(main_image), main_image->depth, main_image->nChannels);
    IplImage *image_gray = cvCreateImage( cvSize(image->width, image->height), 8, 1);

    ///< Копирование области распознавания в отдельное изображение
    cvCopy(main_image, image, NULL);
#endif

    ///< Создание изображения с оттенками серого
    cvCvtColor(image, image_gray, cv::COLOR_BGR2GRAY);    
#ifdef ANDROID_BUILD
    cvSaveImage("/storage/emulated/0/DCIM/image_gray.jpg", image_gray);
#else
    cvSaveImage("image_gray.jpg", image_gray);
    //float kernel[9] = {-0.1,-0.1,-0.1,-0.1,2,-0.1,-0.1,-0.1,-0.1};
//    double kernel[9] = {0.1111, -0.8889, 0.1111, -0.8889, 4.1111, -0.8889, 0.1111, -0.8889, 0.1111};
//    CvMat kernel_matrix = cvMat(3, 3, CV_64FC1, kernel);
//    cvFilter2D(image_gray, image_gray, &kernel_matrix, cvPoint(-1,-1));
//    cvSaveImage("image_gray_ex.jpg", image_gray);
    //cvAddWeighted(image_gray, 2, image_gray, -0.1, 0, image_gray);
    cvAddWeighted(image_gray, 2.5, image_gray, -0.1, 0, image_gray);
    cvSaveImage("image_gray_ex.jpg", image_gray);
    cvEqualizeHist(image_gray, image_gray);
    cvSaveImage("image_gray_ex2.jpg", image_gray);
#endif


    ///< Бинаризация изображения
    //IplImage *image_binary = cvCreateImage( cvSize(image->width, image->height), 8, 1);
    //cvCanny(image, image_binary, 50, 200);
    //cvSaveImage("image_binary.jpg", image_binary);
    //cvThreshold(image_gray, image_gray, 110, 255, CV_THRESH_BINARY);
    cvAdaptiveThreshold(image_gray, image_gray, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21, 7);
    //cvCanny(image_gray, image_gray, 50, 200);
#ifdef ANDROID_BUILD
    cvSaveImage("/storage/emulated/0/DCIM/image_binary.jpg", image_gray);
#else
    cvSaveImage("image_binary.jpg", image_gray);
#endif

    ///< Нахождение контуров
    CvSeq *contours = 0;
    CvMemStorage *storage = cvCreateMemStorage(0);
    cvFindContours(image_gray, storage, &contours, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
    //cvFindContours(image_gray, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
    //cvFindContours(image_gray, storage, &contours, sizeof(CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
    //cvFindContours(image_gray, storage, &contours, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

    ///< Удаление мелких контуров
//    CvSeq *h_next = NULL;
//    for (CvSeq *c = contours; c != NULL; c = c->h_next) {
//        if (c != contours) {
//            if (c->total <= 30) {
//                h_next->h_next = h_next->h_next->h_next;
//                continue;
//            }
//        }
//        h_next = c;
//    }
//    if (contours->total <= 30) contours = contours->h_next;

    ///< Иницаизация шрифта и Нумерование контуров
    int num = 0;
    char text_buf[8];
    cv::Rect rect;
    CvFont my_font;
    cvInitFont(&my_font, CV_FONT_HERSHEY_PLAIN, 0.7, 1, 0, 1, 8);
    for (CvSeq *c = contours; c != NULL; c = c->h_next) {
        sprintf(text_buf, "%d", num);
        rect = cvBoundingRect(c);
        cvPutText(image, text_buf, cvPoint(rect.tl().x,rect.tl().y), &my_font, cvScalar(150,30,150));
        ++num;
    }

//    CvSeq *tmp_tree = NULL;
//    QList<CvSeq*> list;
//    list.push_back(contours);

//    while (!list.empty()) {
//        tmp_tree = list.front();
//        list.pop_front();
//        while (tmp_tree != NULL) {

//            if (tmp_tree->total < 30 /*cvBoundingRect(tmp_tree).height < 20 && cvBoundingRect(tmp_tree).width < 20*/) {
//                tmp_tree = tmp_tree->h_next;
//                //continue;
//            }
//            else {

//            if (tmp_tree->v_next != NULL)
//                list.push_back(tmp_tree->v_next);

//            //do whith element
//            sprintf(text_buf, "%d", num);
//            rect = cvBoundingRect(tmp_tree);
//            cvPutText(image, text_buf, cvPoint(rect.tl().x,rect.tl().y), &my_font, cvScalar(150,30,150));
//            ++num;


//            qDebug() << cvBoundingRect(tmp_tree).height;

//            tmp_tree = tmp_tree->h_next;
//            }
//        }
//        qDebug() << "next level";
//    }

    ///< Сглаживание контуров
    contours = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 3, 1);

    ///< Зарисовка контуров на исходном изображении
    cvDrawContours(image, contours, CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), 2, 1, CV_AA, cvPoint(0,0));
#ifdef ANDROID_BUILD
    cvSaveImage("/storage/emulated/0/DCIM/image_contours.jpg", image);
#else
    cvSaveImage("image_contours.jpg", image);
#endif


    ///< Получение моментов контуров области распознавания (recognition area)
    CvMoments *moments_ra = new CvMoments[num];
    CvHuMoments *HuMoments_ra = new CvHuMoments[num];
    int i = 0;
    for (CvSeq *c = contours; c != NULL; c = c->h_next) {
        cvMoments(c, &moments_ra[i]);
        cvGetHuMoments(&moments_ra[i], &HuMoments_ra[i]);
        ++i;
    }


//#ifdef PC_BUILD
    ///< INIT
    ///< Нахождение и отображение контуров эталонных цифр
    IplImage *img_size = cvLoadImage("digits_img/0.png", 1);
#ifdef PC_BUILD
    QString img_base = "digits_img/"; //+ i + ".png"
#else
    QString img_base = "/storage/emulated/0/digits_img/";
#endif
    QString res = "";

    IplImage *d0;
    IplImage *d0_bin;

    CvMemStorage *storage_t = cvCreateMemStorage(0);
    CvSeq *contours_t[10];
    CvMoments moments_t[10];
    CvHuMoments HuMoments_t[10];


    for (int i = 0; i < 10; ++i) {
        res.append(img_base + QString::number(i) + ".png");

        d0 = cvLoadImage(res.toStdString().c_str(), 1);
        d0_bin = cvCreateImage( cvGetSize(d0), 8, 1);
        cvCvtColor(d0, d0_bin, cv::COLOR_BGR2GRAY);

        //cvCanny(d0_bin, d0_bin, 50, 200);
        cvAdaptiveThreshold(d0_bin, d0_bin, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21, 7);
        cvFindContours(d0_bin, storage_t, &contours_t[i], sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

        cvMoments(contours_t[i]->v_next, &moments_t[i]);
        cvGetHuMoments(&moments_t[i], &HuMoments_t[i]);

        cvReleaseImage(&d0);
        cvReleaseImage(&d0_bin);
        res.clear();
    }
//#endif


#ifdef PC_BUILD1
    ///< Тестовый
    //d0 = cvLoadImage("digits_img/1.png", 1);
    //d0_bin = cvCreateImage( cvGetSize(d0), 8, 1);
    //cvCvtColor(d0, d0_bin /*gray, cv::COLOR_BGR2GRAY);  //!!!!!!!!!!!!!!!!!!!!!!
    //cvCanny(d0_bin, d0_bin, 50, 200);

    //cvFindContours(d0_bin, storage_t, &contours_t, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));
    //cvDrawContours(d0, contours_t[1], CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), 2, 1, CV_AA, cvPoint(0,0));

    //std::vector<std::vector<cv::Point> contours2;
    //std::vector<cv::Vec4i> hierarchi;
    CvMemStorage *storage2 = cvCreateMemStorage(0);
    CvSeq *contours2;

    d0 = cvLoadImage("4scale2.jpg", 1);
    //d0 = cvLoadImage(":/digits/digits_img/7.jpg", 1);
    d0_bin = cvCreateImage( cvGetSize(d0), 8, 1);
    cvCvtColor(d0, d0_bin, cv::COLOR_BGR2GRAY);
    //cvCanny(d0_bin, d0_bin, 50, 200);
    //cvThreshold(d0_bin, d0_bin, 1, 255, CV_THRESH_BINARY_INV);
    cvAdaptiveThreshold(d0_bin, d0_bin, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21, 7);
    cvFindContours(d0_bin, storage2, &contours2, sizeof(CvContour), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0));

    //cvDrawContours(d0, contours2, CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), 1, 1, CV_AA, cvPoint(0,0));
    //std::vector<cv::Point> vec;
    //CvArr *arr[1000];
    //cvCvtSeqToArray(contours2, arr);

    //cv::cvarrToMat(contours2);

    int max = contours2->total;
    CvSeq *max_pos = contours2->v_next;
//    CvSeq *max_pos = contours2;
//    for (CvSeq *c = contours2; c != NULL; c = c->h_next) {
//        if (c->total >= max ) { max = c->total; max_pos = c; } //if more than max pos, save it
//    }

    cvDrawContours(d0, max_pos, CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), 1, 1, CV_AA, cvPoint(0,0));
    cvDrawContours(d0, contours_t[7]->v_next, CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), 1, 1, CV_AA, cvPoint(0,0));

    cv::Rect bbox = cvBoundingRect(max_pos);
    qDebug() << bbox.tl().x << bbox.tl().y;
    qDebug() << bbox.br().x << bbox.br().y;

    cvRectangle(d0, bbox.tl(), bbox.br(), CV_RGB(150, 30, 150), 2, 8, 0);

    cvNamedWindow("Digit", 1);
    cvShowImage("Digit", d0);

    ///< Получение моментов контура
    CvMoments moments;
    CvHuMoments HuMoments;

    cvMoments(max_pos, &moments);
    cvGetHuMoments(&moments, &HuMoments);

    ///< Cравнение моментов
    double result = cvMatchShapesHuMoments(HuMoments_t[6], HuMoments, CV_CONTOURS_MATCH_I3);
    qDebug() << "cvMatchShapes() Result: " << result;
#endif

//    CvSeq *tmp_tree = NULL;
//    QList<CvSeq*> list;
//    list.push_back(contours);

//    while (!list.empty()) {
//        tmp_tree = list.front();
//        list.pop_front();
//        while (tmp_tree != NULL) {

//            if (tmp_tree->total < 30) {
//                tmp_tree = tmp_tree->h_next;
//                //continue;
//            }
//            else {

//                if (tmp_tree->v_next != NULL)
//                    list.push_back(tmp_tree->v_next);

//                //do whith element


//                qDebug() << cvBoundingRect(tmp_tree).height;

//                tmp_tree = tmp_tree->h_next;
//            }
//        }
//        qDebug() << "next level";
//    }

    CvSeq *tmp_tree = NULL;
    QList<CvSeq*> list;
    list.push_back(contours);

    while (!list.empty()) {
        tmp_tree = list.front();
        list.pop_front();
        while (tmp_tree != NULL) {

            if (/*tmp_tree->total < 5*/ cvBoundingRect(tmp_tree).height < 20 || cvBoundingRect(tmp_tree).width < 20) {
                tmp_tree = tmp_tree->h_next;
                //continue;
            }
            else {

                if (tmp_tree->v_next != NULL)
                    list.push_back(tmp_tree->v_next);

                //do whith element
                qDebug() << cvBoundingRect(tmp_tree).height;




                tmp_tree = tmp_tree->h_next;
            }
        }
        qDebug() << "next level";
    }

    return 0;

    int index = 0;
    int *h_tmp = new int[num];
    for (CvSeq *c = contours; c != NULL; c = c->h_next) {
        h_tmp[index] = cvBoundingRect(c).height;
        qDebug() << cvBoundingRect(c).height;
        ++index;
    }

    ///< Нахождение стоимости на ценнике путем перебора и сравнения моментов контуров (нахождение 4 одинаковых по высоте контура)
    int n = 1;
    //int *h_tmp = new int[num];
    CvSeq *c_tmp[] = {NULL,NULL,NULL,NULL};
    //CvSeq *c = NULL;
    //int index = 0;
    for (CvSeq *c = contours->h_next; c != NULL; c = c->h_next) {
        h_tmp[index] = cvBoundingRect(c).height;
        qDebug() << cvBoundingRect(c).height;
        ++index;
        if (n == 3) {
            // Do something...0
            // Save contours in array
            c_tmp[2] = contours->h_prev;
            c_tmp[1] = contours->h_prev->h_prev;
            c_tmp[0] = contours->h_prev->h_prev->h_prev;
            //c_tmp[0] = contours->h_prev->h_prev->h_prev->h_prev;
            break;
        }
        if (ApproxEqu(c, c->h_prev, 2))
            ++n;
        else
            n = 1;
    }

    // Get HuMoments from saved contours
    CvMoments m_tmp[4];
    CvHuMoments hm_tmp[4];

    if (c_tmp[0] == NULL)
        return 0;

    for (int i = 0; i < 3; ++i) {
        cvMoments(c_tmp[i], &m_tmp[i]);
        cvGetHuMoments(&m_tmp[i], &hm_tmp[i]);
    }


    // Compare each HuMoment with cipher HuMoments.
    QString sum;
    //double sum;
    int cipher = 0;
    double rec_res = 10;
    double rec_tmp = 0;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 10; ++j) {
            rec_tmp = cvMatchShapesHuMoments(HuMoments_t[j], hm_tmp[i], CV_CONTOURS_MATCH_I3);
            if (rec_tmp < rec_res) {
                rec_res = rec_tmp;
                cipher = j;
            }
        }
        sum.append(QChar(cipher));
        rec_res = 10;
    }

    qDebug() << "recognition sum:" << sum;


#ifdef PC_BUILD
    ///< Ожидание нажатия клавиши
    cvWaitKey(0);
#endif

    ///< Освобождение ресурсов
    cvReleaseMemStorage(&storage);    
    cvReleaseImage(&image_gray);
    cvReleaseImage(&image);

#ifdef PC_BUILD
    cvReleaseMemStorage(&storage_t);
    cvReleaseImage(&d0);
    cvReleaseImage(&d0_bin);
#endif

#ifdef PC_BUILD
    ///< Закрытие окон
    cvDestroyAllWindows();
#endif

    return 0.0;    
}

void Scanner::Test()
{
    IplImage *image_gray = cvCreateImage( cvSize(300, 300), 8, 1);
    cvSaveImage("/sdcard/DCIM/testimage.jpg", image_gray);
    cvReleaseImage(&image_gray);
}

double Scanner::cvMatchShapesHuMoments(CvHuMoments HuMoments1, CvHuMoments HuMoments2, int method)
{
    double ma[7], mb[7];
    int i, sma, smb;
    double eps = 1.e-5;
    double mmm;
    double result = 0;

    ma[0] = HuMoments1.hu1;
    ma[1] = HuMoments1.hu2;
    ma[2] = HuMoments1.hu3;
    ma[3] = HuMoments1.hu4;
    ma[4] = HuMoments1.hu5;
    ma[5] = HuMoments1.hu6;
    ma[6] = HuMoments1.hu7;

    mb[0] = HuMoments2.hu1;
    mb[1] = HuMoments2.hu2;
    mb[2] = HuMoments2.hu3;
    mb[3] = HuMoments2.hu4;
    mb[4] = HuMoments2.hu5;
    mb[5] = HuMoments2.hu6;
    mb[6] = HuMoments2.hu7;

    switch (method) {
    case 1:{
        for (i = 0; i < 7; i++) {
            double ama = fabs( ma[i] );
            double amb = fabs( mb[i] );

            if (ma[i] > 0)
                sma = 1;
            else if (ma[i] < 0)
                sma = -1;
            else
                sma = 0;
            if (mb[i] > 0)
                smb = 1;
            else if (mb[i] < 0)
                smb = -1;
            else
                smb = 0;

            if (ama > eps && amb > eps) {
                ama = 1. / (sma * log10(ama));
                amb = 1. / (smb * log10(amb));
                result += fabs(-ama + amb);
            }
        }
        break;
    }

    case 2: {
        for (i = 0; i < 7; i++) {
            double ama = fabs( ma[i] );
            double amb = fabs( mb[i] );

            if (ma[i] > 0)
                sma = 1;
            else if (ma[i] < 0)
                sma = -1;
            else
                sma = 0;
            if (mb[i] > 0)
                smb = 1;
            else if (mb[i] < 0)
                smb = -1;
            else
                smb = 0;

            if (ama > eps && amb > eps) {
                ama = sma * log10(ama);
                amb = smb * log10(amb);
                result += fabs(-ama + amb);
            }
        }
        break;
    }

    case 3: {
        for (i = 0; i < 7; i++) {
            double ama = fabs( ma[i] );
            double amb = fabs( mb[i] );

            if (ma[i] > 0)
                sma = 1;
            else if (ma[i] < 0)
                sma = -1;
            else
                sma = 0;
            if (mb[i] > 0)
                smb = 1;
            else if (mb[i] < 0)
                smb = -1;
            else
                smb = 0;

            if (ama > eps && amb > eps) {
                ama = sma * log10(ama);
                amb = smb * log10(amb);
                mmm = fabs((ama-amb) / ama);
                if (result < mmm)
                    result = mmm;
            }
        }
        break;
    }

    //default:
        //CV_Error(CV_StsBadArg, "Unknown comparation method");
    }

    return result;
}

bool Scanner::ApproxEqu(CvArr *points1, CvArr *points2, int difference)
{
    int mod = abs(cvBoundingRect(points1).height - cvBoundingRect(points2).height);
    if (mod < difference)
        return true;
    else
        return false;
}


//void Scanner::rotate(const cv::Mat &input, cv::Mat &output, double angle)
//{
//    cv::Point2f src_center(input.cols/2.0F, input.rows/2.0F);
//    cv::Mat rot_matrix = cv::getRotationMatrix2D(src_center, angle, 1.0);
//    output.create(cv::Size(input.cols, input.rows), input.type());
//    cv::warpAffine(input, output, rot_matrix, input.size());
//}


