import QtQuick 2.0

Item {
    id: item
    width: 100
    height: 40
    property string borderColor: "gray"

    Rectangle {
        anchors.fill: parent
        color: "transparent"
        border.color: borderColor
        border.width: 1
        radius: 5
        Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 10
            id: priceText
            text: model.price
            color: "black"
            font.pixelSize: parent.height/2
        }
        Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
            id: countText
            text: "x" + model.count
            color: "black"
            font.pixelSize: parent.height/2
        }
    }
}

