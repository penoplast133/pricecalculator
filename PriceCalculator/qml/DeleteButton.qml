import QtQuick 2.2

TouchArea {
    Rectangle {
        color: "red"
        width: parent.width / 2
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2
        radius: 3
        rotation: 30
    }
    Rectangle {
        color: "red"
        width: parent.width / 2
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2
        radius: 3
        rotation: -30
    }
}
