import QtQuick 2.2
import "Utility.js" as Utility

Rectangle {
    id: multiTouchArea
    color: "#00000000"
    border.color: "black"
    border.width: 1

    MultiPointTouchArea {
        anchors.fill: parent
        maximumTouchPoints: 2
        minimumTouchPoints: 1
        touchPoints: [
            TouchPoint { id: point1 },
            TouchPoint { id: point2 }
        ]
        onPressed: {
            if (touchPoints.length === 2) {
                Utility.beginDist = Utility.distance(point1.x,point1.y,point2.x,point2.y);

                //! New MultiTouch Area Support
                Utility.beginX1 = point1.x; Utility.beginY1 = point1.y;
                Utility.beginX2 = point2.x; Utility.beginY2 = point2.y;
            }
            if (touchPoints.length === 1) {
                Utility.deltaX = point1.sceneX-multiTouchArea.x;
                Utility.deltaY = point1.sceneY-multiTouchArea.y;
            }
            console.log(touchPoints.length)
        }
        onUpdated: {
            if (touchPoints.length === 2) {
                Utility.deltaDist = Utility.distance(point1.x,point1.y,point2.x,point2.y) - Utility.beginDist;
                multiTouchArea.width += Utility.deltaDist;
                multiTouchArea.height += Utility.deltaDist;
                /*???*/Utility.beginDist = Utility.distance(point1.x,point1.y,point2.x,point2.y);

                //! New MultiTouch Area Support
                multiTouchArea.width += (point1.x - Utility.beginX1)
                multiTouchArea.width += (point2.x - Utility.beginX2)
                multiTouchArea.height += (point1.y - Utility.beginY1)
                multiTouchArea.height += (point2.y - Utility.beginY2)
            }
            if (touchPoints.length === 1) {
                multiTouchArea.x = point1.sceneX - Utility.deltaX;
                multiTouchArea.y = point1.sceneY - Utility.deltaY;
            }
        }
    }
}
