import QtQuick 2.2

TouchArea {
    Rectangle {
        id: photoRec
        color: parent.color
        width: parent.width / 2
        height: parent.height / 1.7
        border.color: parent.border.color
        border.width: 3
        anchors.centerIn: parent
        radius: 5

        Rectangle {
            color: parent.color
            width: parent.width / 2.5
            height: parent.height / 2
            border.color: parent.border.color
            border.width: 2
            anchors.centerIn: parent
            radius: 5
        }
        Rectangle {
            color: parent.color
            width: parent.width / 9
            height: parent.width / 9
            border.color: parent.border.color
            border.width: 1
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 4
            anchors.rightMargin: 4
            radius: 2
        }
    }
}
