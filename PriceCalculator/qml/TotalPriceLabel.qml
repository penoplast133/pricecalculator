import QtQuick 2.0

TouchArea {
    property string text: "0.00"

    Text {
        id: txt
        text: parent.text
        color: parent.border.color
        font.pixelSize: parent.width / 8
        anchors.centerIn: parent
    }
}
