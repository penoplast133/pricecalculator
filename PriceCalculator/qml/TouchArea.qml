import QtQuick 2.2
import QtQuick.Window 2.1

Rectangle {
    id: root
    color: "pink"
    border.width: 1
    border.color: "gray"
    radius: 2

    signal buttonClick()

    MouseArea {
        anchors.fill: parent
        onClicked: buttonClick()
        //hoverEnabled: true
    }
}
