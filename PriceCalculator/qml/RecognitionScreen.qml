import QtQuick 2.2
import QtQuick.Window 2.1
import QtMultimedia 5.0
import ImageProcessor 1.0


Rectangle {    
    id: blueRec
    visible: true
    color: "lightblue"
    anchors.fill: parent
    property string img_path;
    signal menuChange()    

    VideoOutput {
        id: videoOutput        
        anchors.fill: parent
        visible: true
        width: parent.width
        height: parent.height - (parent.height / 9)
        anchors.left: parent.left
        anchors.top: parent.top
        fillMode: Image.PreserveAspectCrop
        orientation: 360
        source: camera
        Image {id: photo; visible: false;}
        Camera {
            id: camera
            flash.mode: Camera.FlashRedEyeReduction
            imageCapture {                
                onImageCaptured: {photo.source = preview}
                onImageSaved: {
                    img_path = path;
                    debugText.text = path
                    //console.log("/sdcard/DCIM/" + img_path); // /storage/emulated/0/DCIM/
                    img_proc.processImage(mta.x / (videoOutput.width/100),
                                          mta.y / (videoOutput.height/100),
                                          (mta.x+mta.width) / (videoOutput.width/100),
                                          (mta.y+mta.height) / (videoOutput.height/100),
                                          path);
                }
            }
        }
    }

    Text {
        id: debugText
        width: blueRec.width / 2
        height: blueRec.height / 10
        color: "red"
        text: "dbg" //"/sdcard/DCIM/"
    }

    MultiTouchArea {
        id: mta
        width: blueRec.width / 1.5
        height: blueRec.height / 5
        x: blueRec.width / 6
        y: blueRec.height / 3
        border.color: "#80FF00"
    }

    TakeSnapshotButton {
        id: leftTouchArea
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: blueRec.left
        anchors.bottom: blueRec.bottom
        onButtonClick: { camera.imageCapture.capture() }
    }

    TotalPriceLabel {
        id: midTouchArea        
        width: blueRec.width / 2
        height: blueRec.height / 10
        anchors.left: leftTouchArea.right
        anchors.bottom: blueRec.bottom
        //text: priceModel.getTotalPrice();
        text: priceModel.totalPrice
    }

    PriceMenuButton {
        id: rightTouchArea
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: midTouchArea.right
        anchors.bottom: blueRec.bottom
        onButtonClick: { menuChange(); console.log("Recognition: clicked..."); }
    }

    ImageProcessor {
        id: img_proc
    }
}


//    MultiTouchArea {
//        width: blueRec.width / 1.5
//        height: blueRec.height / 5
//        x: blueRec.width / 6
//        y: blueRec.height / 3
//    }

//    Item {
//        id: outItem
//        height: parent.height - leftTouchArea.height;
//        width: parent.width;
//        anchors.left: parent.left
//        anchors.top: parent.top
//    }
