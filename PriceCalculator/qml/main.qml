import QtQuick 2.2
import QtQuick.Window 2.1


Window {
    id: root
    visible: true
    width: 360
    height: 360

    EditorScreen {
        id: editorScreen
        width: root.width; height: root.height; opacity: 0
        onMenuChange: { editorFade.start(); }
        //onMenuChange: { editorScreen.visible = false; cameraScreen.visible = true; }
    }
    RecognitionScreen {
        id: cameraScreen
        width: root.width; height: root.height;
        onMenuChange: { cameraFade.start(); }
        //onMenuChange: { cameraScreen.visible = false; editorScreen.visible = true; }
    }

    SequentialAnimation {
        id: cameraFade
        running: false
        NumberAnimation { target: cameraScreen; property: "opacity"; to: 0.5; duration: 100 }
        NumberAnimation { target: editorScreen; property: "opacity"; to: 1; duration: 100 }
        onStopped: { cameraScreen.visible = false; }
        onStarted: { editorScreen.visible = true; }
    }

    SequentialAnimation {
        id: editorFade
        running: false
        NumberAnimation { target: editorScreen; property: "opacity"; to: 0.5; duration: 100 }
        NumberAnimation { target: cameraScreen; property: "opacity"; to: 1; duration: 100 }
        onStopped: { editorScreen.visible = false; }
        onStarted: { cameraScreen.visible = true; }
    }
}






//Window {
//    id: root
//    visible: true
//    width: 360
//    height: 360

//    ListView {
//        id: menuListView
//        anchors.fill: parent
//        width: parent.width
//        height: parent.height
//        model: menuListModel
//        //snapMode: ListView.SnapOneItem
//        orientation: ListView.Horizontal

////        boundsBehavior: Flickable.StopAtBounds
////        flickDeceleration: 5000
////        highlightFollowsCurrentItem: true
////        highlightMoveDuration: 240
////        highlightRangeMode: ListView.StrictlyEnforceRange
//    }

//    VisualItemModel {
//        id: menuListModel

//        RecognitionScreen {
//            id: cameraScreen
//            width: root.width; height: root.height;
//            onMenuChange: ListView.currentIndex = 1
//        }
//        EditorScreen {
//            id: editorScreen
//            width: root.width; height: root.height;
//            onMenuChange: ListView.currentIndex = 0
//        }
//    }
//}






//import QtQuick 2.1
//import QtQuick.Controls 1.0

//Window {
//    width: 480
//    height: 640
//    visible: true
//    property alias stackView: stackView
//    property alias componentPage: componentPage

//    StackView {
//        id: stackView
//        anchors.fill: parent
//        initialItem: componentPage
//    }

//    Component {
//        id: componentPage
//        Rectangle {
//            color: "yellow"
//            ButtonMenu {
//                index: parent.Stack.index
//            }
//        }
//    }
//}
