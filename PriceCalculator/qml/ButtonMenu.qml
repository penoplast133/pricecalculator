import QtQuick 2.1
import QtQuick.Controls 1.0

Column {
    property int index: -1
    Label {
        text: "stack index: " + index
    }
    Button {
        text: "push Component"
        onClicked: stackView.push(componentPage)
    }
    Button {
        text: "push URL"
        onClicked: stackView.push(Qt.resolvedUrl("View.qml"))
    }
    Button {
        text: "pop"
        onClicked: stackView.pop()
    }
}
