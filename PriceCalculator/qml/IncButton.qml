import QtQuick 2.2

TouchArea {
    Rectangle {
        color: "black"
        width: parent.width / 3
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2
        radius: 3
    }
    Rectangle {
        color: "black"
        width: parent.width / 3.3
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2
        radius: 3
        rotation: 90
    }
}
