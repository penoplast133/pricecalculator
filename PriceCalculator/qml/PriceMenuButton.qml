import QtQuick 2.2

TouchArea {
    Rectangle {
        id: centralLine
        color: parent.border.color
        width: parent.width / 2
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2
        radius: 2
    }
    Rectangle {
        color: parent.border.color
        width: parent.width / 2
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: centralLine.top
        anchors.bottomMargin: parent.height / 10
        radius: 2
    }
    Rectangle {
        color: parent.border.color
        width: parent.width / 2
        height: parent.height / 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: centralLine.bottom
        anchors.topMargin: parent.height / 10
        radius: 2
    }
}
