import QtQuick 2.2
import QtQuick.Window 2.1
import "Utility.js" as Utility


Rectangle {    
    id: blueRec
    visible: true
    color: "lightblue"
    anchors.fill: parent
    signal menuChange()

    GridView {
        id: view
        model: priceModel
        delegate: PriceDelegate {width: parent.width/2.1; height: view.height/9;}
        cellHeight: parent.height/9;
        cellWidth: parent.width/2
        width: blueRec.width
        height: blueRec.height - (blueRec.height / 10)
        anchors.left: blueRec.left
        anchors.top: blueRec.top
        highlight: Rectangle {color: "pink"; radius: 5;}
        currentIndex: -1;
        MouseArea {
            anchors.fill: parent
            onClicked: {                
                if (view.itemAt(mouseX, mouseY) === null)
                    return;
                if (view.currentIndex === view.indexAt(mouseX, mouseY))
                    view.currentIndex = -1;
                else
                    view.currentIndex = view.indexAt(mouseX, mouseY);
            }
        }
    }

    Text {
        id: sumText
        text: priceModel.totalPrice
        font.pointSize: 15
        anchors.bottom: incButton.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    DeleteButton {
        id: deleteButton
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: blueRec.left
        anchors.bottom: blueRec.bottom
        onButtonClick: {
            if (view.currentIndex === -1) return;
            priceModel.deleteElement(view.currentIndex);
            view.currentIndex = -1;            
        }
    }

    IncButton {
        id: incButton
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: deleteButton.right
        anchors.bottom: blueRec.bottom
        onButtonClick: {
            if (view.currentIndex === -1) return;
            priceModel.incCount(view.currentIndex);            
        }
    }

    DecButton {
        id: decButton
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: incButton.right
        anchors.bottom: blueRec.bottom
        onButtonClick: {
            if (view.currentIndex === -1) return;
            priceModel.decCount(view.currentIndex);            
        }
    }

    TakeSnapshotButton {
        id: snapshotButton
        width: blueRec.width / 4
        height: blueRec.height / 10
        anchors.left: decButton.right
        anchors.bottom: blueRec.bottom
        onButtonClick: { menuChange(); console.log("Editor: clicked..."); }
    }
}
