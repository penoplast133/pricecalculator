#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQmlContext>

#include <opencv2/core/core.hpp>
#include <time.h>
#include <QDebug>

#include <Scanner.h>
#include <ImageProcessor.h>
#include "PriceDB.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<ImageProcessor>("ImageProcessor", 1, 0, "ImageProcessor");    

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    PriceDB model;

    model.addElement(32.00);
    model.addElement(11.90);
    model.addElement(70.50);
    model.addElement(205.00);
    model.addElement(999.00);

    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("priceModel", &model);
    engine.load(QUrl(QStringLiteral("qrc:///qml/main.qml")));

    cv::RNG rng(time(NULL));
    qDebug() << QString().number(rng.uniform(0,100));

    //Scanner scanner("sign.jpg");
    //scanner.Scan(50, 50, 80, 80);
    //scanner.Scan(0,0,0,0);

    return app.exec();
}
