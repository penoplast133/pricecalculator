#ifndef PRICEDB_H
#define PRICEDB_H

#include "PriceElement.h"
#include <QAbstractListModel>
#include <QList>

class PriceDB : public QAbstractListModel
{
    Q_OBJECT    
    Q_PROPERTY(QString totalPrice READ totalPrice NOTIFY totalPriceChanged)
public:
    PriceDB();
    virtual ~PriceDB();
    QString totalPrice() const;
    Q_INVOKABLE void addElement(float price);
    Q_INVOKABLE bool deleteElement(int pos);
    Q_INVOKABLE void clearElements();
    Q_INVOKABLE void incCount(int pos);
    Q_INVOKABLE void decCount(int pos);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;

public:
    enum PriceRoles {
        idRole    = Qt::UserRole + 1,
        countRole = Qt::UserRole + 2,
        priceRole = Qt::UserRole + 3,
    };

signals:
    void totalPriceChanged();

private:
    QList<PriceElement> price_list;
    float total_price;    
};

#endif // PRICEDB_H
