#ifndef PRICEELEMENT_H
#define PRICEELEMENT_H

class PriceElement
{
public:    
    PriceElement(int _id): id(_id), count(1), price(0) { calculateSum(); }
    PriceElement(int _id, float _price): id(_id), count(1), price(_price) { calculateSum(); }
    PriceElement(int _id, int _count, float _price): id(_id), count(_count), price(_price) { calculateSum(); }

    inline void setId(int _id) { id = _id; }
    inline void setCount(int _count) { count = _count; calculateSum(); }
    inline void setPrice(float _price) { price = _price; calculateSum();}
    inline void incCount() { ++count; calculateSum(); }

    inline bool decCount() {
        if (count > 0) {--count; calculateSum(); return true;}
        else return false;
    }

    inline int getId() { return id;}
    inline int getCount() { return count; }
    inline float getPrice() { return price; }
    inline float getSum() { return sum; }

//private:
    inline void calculateSum() { sum = price * count; }

//private:
    int id;
    int count;
    float price;
    float sum;
};


#endif // PRICEELEMENT_H
