/*!
 * \brief The Scanner class
 */

#ifndef SCANNER_H
#define SCANNER_H

#include <opencv/cv.h>
#include <opencv/highgui.h>
//#include <opencv/cxcore.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/core/types_c.h>
#include <QString>
#include <string>
#include <vector>
#include <QDebug>

class Scanner
{
public:
    Scanner(const char *img_path);
    float Scan(int x1p, int y1p, int x2p, int y2p);
    void Test();

private:
    const char *file_name;
    double cvMatchShapesHuMoments(CvHuMoments HuMoments1, CvHuMoments HuMoments2, int method);
    bool ApproxEqu(CvArr *points1, CvArr *points2, int difference);
    //bool cvMotionRestore(IplImage* Src,IplImage* Dst);
    //void rotate(const cv::Mat& input, cv::Mat& output, double angle);

};

#endif // SCANNER_H
