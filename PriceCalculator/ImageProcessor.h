#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QObject>
#include <Scanner.h>

class ImageProcessor : public QObject
{
    Q_OBJECT

public:
    explicit ImageProcessor(QObject *parent = 0);

public slots:
    double processImage(int x1p, int y1p, int x2p, int y2p, const QString& image);
};

#endif // IMAGEPROCESSOR_H
